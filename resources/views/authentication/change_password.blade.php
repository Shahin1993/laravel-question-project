<!DOCTYPE html>
<html>
<head>
	<title>Password change</title>
</head>
<body>


	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


@if(Session()->has('error'))
      <div style="color: red;">
      	{{Session()->get('error')}}
      </div>
	@endif

	@if(Session()->has('success'))
      <div style="color: green;">
      	{{Session()->get('success')}}
      </div>
	@endif
  
  <form action="/password-change" method="POST">
   
    {{csrf_field()}}

    <input type="password" name="old_password" placeholder="Old Password">
    <input type="password" name="password" placeholder="new Password">
    <input type="password" name="confirm_password" placeholder="confirm  Password">

    <input type="submit" value="Password Change & Save ">

  	
  </form>

</body>
</html>