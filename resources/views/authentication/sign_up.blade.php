<!DOCTYPE html>
<html>
<head>
	<title>Sign up</title>
</head>
<body>

	
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


	@if(Session()->has('success'))
      <div style="color: green;">
      	{{Session()->get('success')}}
      </div>
	@endif
    


	<form action="/sign-up" method="POST">
		{{csrf_field()}}
		<input type="text" name="first_name" placeholder="first name" required="">
		<input type="text" name="last_name" placeholder="last name">
		<input type="text" name="email" placeholder="email" required="">
		<input type="password" name="password" placeholder="password" required="">
		<input type="password" name="confirm_password" placeholder="confirm password" required="">
		<input type="submit" value="Submit">
		
	</form>

</body>
</html>