<!DOCTYPE html>
<html>
<head>
	<title>Department edit</title>
</head>
<body>

	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


	@if(Session()->has('success'))
      <div style="color: green;">
      	{{Session()->get('success')}}
      </div>
	@endif
    


    <h3><a href="/departments">View</a></h3>

  <form action="/departments/{{$data->id}}" method="POST">
  	{{csrf_field()}}

  	<input type="text" name="dpt_name" placeholder="Department name" value="{{$data->dpt_name}}">
  	<input type="text" name="dpt_code" placeholder="Department code" value="{{$data->dpt_code}}">
    
  	<input type="submit" value="Update">

  </form>

   
</body>
</html>