<!DOCTYPE html>
<html>
<head>
	<title>View</title>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

</head>
<body>
<div class="container">
	<table border="1">
  	<tr>
  		<th>SL</th>
  		<th>Dpt Name</th>
  		<th>Dpt Code</th>
  		<th>Action</th>
  	</tr>
  	<?php $i=$dpt->perPage()*($dpt->currentPage()-1);  ?>
  	@foreach($dpt as  $data)
     <tr>
     	<td> <?php $i++ ?>  {{$i}}</td>
     	<td>{{$data->dpt_name}}</td>
     	<td>{{$data->dpt_code}}</td>
     	<td><a href="/departments/{{$data->id}}/edit">Edit</a> | <a href="/department-delete/{{$data->id}}">Delete</a></td>
     </tr>
  	@endforeach
  </table>

  <br>
 {{$dpt->links()}}
</div>
</body>
</html>