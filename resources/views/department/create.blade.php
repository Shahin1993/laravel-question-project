<!DOCTYPE html>
<html>
<head>
	<title>Department</title>
</head>
<body>

	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


	@if(Session()->has('success'))
      <div style="color: green;">
      	{{Session()->get('success')}}
      </div>
	@endif
    


    <h3><a href="/departments">View</a></h3>

    <h4> <a href="/password-change"> Change Password</a></h4>

  <form action="/departments" method="POST">
  	{{csrf_field()}}

  	<input type="text" name="dpt_name" placeholder="Department name">
  	<input type="text" name="dpt_code" placeholder="Department code">
  	<input type="submit" value="Save">

  </form>
<br><br>
   {{date('Y-m-d h:i A')}}
   <br>
  <table border="1">
  	<tr>
  		<th>SL</th>
  		<th>Dpt Name</th>
  		<th>Dpt Code</th>
      <th>Created at</th>
  		<th>Action</th>
  	</tr>
  	@foreach($dpt as $key=>$data)
     <tr>
     	<td>{{++$key}}</td>
     	<td>{{$data->dpt_name}}</td>
     	<td>{{$data->dpt_code}}</td>
      <td>{{$data->created_at->format('Y-m-d h:i A')}}</td>
  <td><a href="/departments/{{$data->id}}/edit">Edit</a> | <a href="/department-delete/{{$data->id}}" onclick="return confirm('are you sure Delete this data?')">Delete</a></td>
     </tr>
  	@endforeach
  </table>

</body>
</html>