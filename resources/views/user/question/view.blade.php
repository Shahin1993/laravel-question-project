@extends('layouts.frontend.master')
@section('title','Ask Question')
@section('css')
<style type="text/css">
	#myTable tr th{
     font-size: 16px;
	}
	#myTable tr td{
     font-size: 16px;
	}
</style>
@endsection
@section('content')

  <center><h2>My Questions</h2></center>

  <div class="container">

  	<table class="table table-bordered table-hover" id="myTable">
  		<thead>
  			<tr>
  				<th>SL</th>
  				<th>Question Title</th>
  				<th>Details</th>
  				<th>Edit</th>
  				<th>Delete</th>
  			</tr>

  		</thead>
  		<tbody>
  			@if($questions->count()>0)
  			@foreach($questions as $key=>$qus)
            <tr>
            	<td>{{++$key}}</td>
            	<td>{{$qus->title}}</td>
            	<td><a href="/questions/{{$qus->id}}">Details</a></td>
            	<td><a href="/questions/{{$qus->id}}/edit">Edit</a></td>
            	<td>Delete</td>
            </tr>
  			@endforeach
  			@else

  			<tr>
  				<td colspan="5"> Opps!! No Data Found</td>
  			</tr>

  			@endif

  		</tbody>
  		
  	</table>
  	
  </div>

@endsection