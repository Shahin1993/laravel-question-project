@extends('layouts.frontend.master')
@section('title','Ask Question')
@section('content')
<section class="container main-content">
<div class="row">
<div class="col-md-9">

<div class="page-content ask-question">
<div class="boxedtitle page-title"><h2>Ask Question</h2></div>

 

<p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.</p>

<div class="form-style form-style-3" id="question-submit">
<form action="/questions" method="POST">
	{{csrf_field()}}
	<div class="form-inputs clearfix">
		<p>
			<label class="required">Question Title<span>*</span></label>
			<input type="text" id="question-title" name="title">
			<span class="form-description">Please choose an appropriate title for the question to answer it even easier .</span>
		</p>
		<p>
			<label>Tags</label>
			<input type="text" class="input" name="question_tags" id="question_tags" data-seperator=",">
			<span class="form-description">Please choose  suitable Keywords Ex : <span class="color">question , poll</span> .</span>
		</p>
	  
		<div class="clearfix"></div>
		 
		<label>Attachment</label>
		<div class="fileinputs">
			<input type="file" class="file" name="image">
			<div class="fakefile">
				<button type="button" class="button small margin_0">Select file</button>
				<span><i class="icon-arrow-up"></i>Browse</span>
			</div>
		</div>
		
	</div>
	<div id="form-textarea">
		<p>
			<label class="required">Details<span>*</span></label>
			<textarea  id="summernote" placeholder= "Add your Answer......." name="description"></textarea>

            
			<span class="form-description">Type the description thoroughly and in detail .</span>
		</p>
	</div>

	<input type="hidden" name="user_id" value="{{Sentinel::getUser()->id}}">
	<p class="form-submit">
		<input type="submit" id="publish-question" value="Publish Your Question" class="button color small submit">
	</p>
</form>
</div>
</div><!-- End page-content -->
</div><!-- End main -->

@include('layouts.frontend.partials.sidebar')

</div><!-- End row -->
</section><!-- End container -->
@endsection

@section('js')

    <script type= "text/javascript">

                    $(document).ready(function() {
                        $('#summernote').summernote({
                            height: "200px"
                        });
                    });
                    var postForm = function() {
                        var content = $('textarea[name="u_answer"]').html($('#summernote').code());
                    }
                    </script>

@endsection