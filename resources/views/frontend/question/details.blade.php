	@extends('layouts.frontend.master')
	@section('title', 'Home')

	@section('content')


	<section class="container main-content page-left-sidebar">
	<div class="row">
	<div class="col-md-9">
	<article class="post single-post clearfix">
	<div class="post-inner">
	<div class="post-img"><a href="single_post.html"><img src="../ask-me/images/demo/posts/loneliness-1440x900.jpg" alt=""></a></div>
	<h2 class="post-title"><span class="post-type"><i class="icon-film"></i></span>{{$data->title}}</h2>
	<div class="post-meta">
 
	<span class="meta-date"><i class="icon-time"></i>{{$data->created_at->format('d F, Y h:i A')}}</span>
	 
	<span class="meta-comment"><i class="icon-comments-alt"></i><a href="#">15 Answer</a></span>
	</div>
	<div class="post-content">
	<p>{{$data->description}}</p>
	</div><!-- End post-content -->
	<div class="clearfix"></div>
	</div><!-- End post-inner -->
	</article><!-- End article.post -->

	<div class="share-tags page-content">
 
	<div class="share-inside-warp">
	 
	<span class="share-inside-f-arrow"></span>
	<span class="share-inside-l-arrow"></span>
	</div><!-- End share-inside-warp -->
	<div class="share-inside"><i class="icon-share-alt"></i>Share</div>
	<div class="clearfix"></div>
	</div><!-- End share-tags -->

	<!-- <div class="about-author clearfix">
	<div class="author-image">
	<a href="#" original-title="admin" class="tooltip-n"><img alt="" src="http://2code.info/demo/html/ask-me/images/demo/admin.jpeg"></a>
	</div>   
	<div class="author-bio">
	<h4>About the Author</h4>
	Lorem ipsum dolor sit amet, consectetur adipiscing elit. Sed viverra auctor neque. Nullam lobortis, sapien vitae lobortis tristique.
	</div>
	</div> --><!-- End about-author -->

	 
	<div id="commentlist" class="page-content">
	<div class="boxedtitle page-title"><h2>Comments ( <span class="color">5</span> )</h2></div>
	<ol class="commentlist clearfix">
	<li class="comment">
	<div class="comment-body clearfix"> 
	<div class="avatar"><img alt="" src="http://2code.info/demo/html/ask-me/images/demo/admin.jpeg"></div>
	<div class="comment-text">
	<div class="author clearfix">
	<div class="comment-meta">
	<span>admin</span>
	<div class="date">January 15 , 2014 at 10:00 pm</div> 
	</div>
	<a class="comment-reply" href="#"><i class="icon-reply"></i>Reply</a> 
	</div>
	<div class="text"><p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravida odio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequat. Vivamus vulputate posuere nisl quis consequat.</p>
	</div>
	</div>
	</div>
	 
	 
	</ol><!-- End commentlist -->
	</div><!-- End page-content -->

	<div id="respond" class="comment-respond page-content clearfix">
	<div class="boxedtitle page-title"><h2>Leave a Answer</h2></div>
	<form action="/answers" method="POST" id="commentform" class="comment-form">
 
	<div id="respond-textarea">
	<p>
	<label class="required" for="comment">Answer<span>*</span></label>
	<textarea id="comment" name="answer" aria-required="true" cols="58" rows="10"></textarea>
	</p>
	</div>
	<p class="form-submit">
	<input name="submit" type="submit"   value="Post Answer" class="button small color">
	</p>
	</form>
	</div>

	<div class="post-next-prev clearfix">
	<p class="prev-post">
	<a href="#"><i class="icon-double-angle-left"></i>&nbsp;Prev post</a>
	</p>
	<p class="next-post">
	<a href="#">Next post&nbsp;<i class="icon-double-angle-right"></i></a>                                
	</p>
	</div><!-- End post-next-prev -->
	</div><!-- End main -->
	 
		@include('layouts.frontend.partials.sidebar')
	 <!-- End sidebar -->
	</div><!-- End row -->
	</section><!-- End container -->


	@endsection