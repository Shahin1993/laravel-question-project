@extends('layouts.frontend.master')
@section('title', 'Home')

@section('content')
<div class="section-warp ask-me">
<div class="container clearfix">
<div  >
<div class="row">
	<div class="col-md-3">
		<h2>Welcome to Ask me</h2>
		<p>Duis dapibus aliquam mi, eget euismod sem scelerisque ut. Vivamus at elit quis urna adipiscing iaculis. Curabitur vitae velit in neque dictum blandit. Proin in iaculis neque.</p>
		<div class="clearfix"></div>
		<a class="color button dark_button medium" href="#">About Us</a>
		<a class="color button dark_button medium" href="#">Join Now</a>
	</div>
	<div class="col-md-9">
		<form class="form-style form-style-2">
			<p>
				<textarea rows="4" id="question_title" onfocus="if(this.value=='Ask any question and you be sure find your answer ?')this.value='';" onblur="if(this.value=='')this.value='Ask any question and you be sure find your answer ?';">Ask any question and you be sure find your answer ?</textarea>
				<i class="icon-pencil"></i>
				<span class="color button small publish-question">Ask Now</span>
			</p>
		</form>
	</div>
</div><!-- End row -->
</div><!-- End box_icon -->
</div><!-- End container -->
</div><!-- End section-warp -->

<section class="container main-content">
<div class="row">
<div class="col-md-9">

<div class="tabs-warp question-tab">
    <ul class="tabs">
        <li class="tab"><a href="#" class="current">Recent Questions</a></li>
         
    </ul>
     <div class="tab-inner-warp">

    @foreach($questions as $qts)
   
		<div class="tab-inner" style="padding-bottom: 10px;">
			<article class="question question-type-normal">
				<h2>
					<a href="/questionDetails/{{$qts->id}}">{{$qts->title}}</a>
				</h2>
				<a class="question-report" href="#">Report</a>
				<div class="question-type-main"><i class="icon-question-sign"></i>Question</div>
				<div class="question-author">
					<a href="#" original-title=" " class="question-author-img tooltip-n"><span></span><img alt="" src="../ask-me/images/demo/avatar.png"></a>
				</div>
				<div class="question-inner">
					<div class="clearfix"></div>
					<p class="question-desc"> {{substr($qts->description,0,300)}}... </p>
					<div class="question-details">
						<span class="question-answered question-answered-done"><i class="icon-ok"></i>solved</span>
						<span class="question-favorite"><i class="icon-star"></i>5</span>
					</div>
					<span class="question-category"><a href="#"><i class="icon-folder-close"></i>wordpress</a></span>
					<span class="question-date"><i class="icon-time"></i>4 mins ago</span>
					<span class="question-comment"><a href="/questionDetails/{{$qts->id}}"><i class="icon-comment"></i>5 Answer</a></span>
					<span class="question-view"><i class="icon-user"></i>70 views</span>
					<div class="clearfix"></div>
				</div>
			</article>
			
	    </div>
	
	@endforeach
	</div>
	 
	 
	 
</div><!-- End page-content -->
</div><!-- End main -->
<aside class="col-md-3 sidebar">
<div class="widget widget_stats">
	<h3 class="widget_title">Stats</h3>
	<div class="ul_list ul_list-icon-ok">
		<ul>
			<li><i class="icon-question-sign"></i>Questions ( <span>20</span> )</li>
			<li><i class="icon-comment"></i>Answers ( <span>50</span> )</li>
		</ul>
	</div>
</div>

 

 

 

 

<div class="widget">
	<h3 class="widget_title">Recent Questions</h3>
	<ul class="related-posts">
		<li class="related-item">
			<h3><a href="#">This is my first Question</a></h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<div class="clear"></div><span>Feb 22, 2014</span>
		</li>
		<li class="related-item">
			<h3><a href="#">This Is My Second Poll Question</a></h3>
			<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>
			<div class="clear"></div><span>Feb 22, 2014</span>
		</li>
	</ul>
</div>

</aside><!-- End sidebar -->
</div><!-- End row -->
</section><!-- End container -->


@endsection