
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(Session()->has('warning'))
      <div style="color: red;">
        {{Session()->get('warning')}}
      </div>
  @endif
    

	@if(Session()->has('success'))
      <div style="color: green;">
      	{{Session()->get('success')}}
      </div>
	@endif
    