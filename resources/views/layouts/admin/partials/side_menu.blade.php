
<div class="main-container ace-save-state" id="main-container">
<script type="text/javascript">
try{ace.settings.loadState('main-container')}catch(e){}
</script>

<div id="sidebar" class="sidebar                  responsive                    ace-save-state">
<script type="text/javascript">
try{ace.settings.loadState('sidebar')}catch(e){}
</script>

<div class="sidebar-shortcuts" id="sidebar-shortcuts">
<div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
<button class="btn btn-success">
<i class="ace-icon fa fa-signal"></i>
</button>

<button class="btn btn-info">
<i class="ace-icon fa fa-pencil"></i>
</button>

<button class="btn btn-warning">
<i class="ace-icon fa fa-users"></i>
</button>

<button class="btn btn-danger">
<i class="ace-icon fa fa-cogs"></i>
</button>
</div>

<div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
<span class="btn btn-success"></span>

<span class="btn btn-info"></span>

<span class="btn btn-warning"></span>

<span class="btn btn-danger"></span>
</div>
</div><!-- /.sidebar-shortcuts -->

<ul class="nav nav-list">
<li class="">
<a href="/abf">
<i class="menu-icon fa fa-tachometer"></i>
<span class="menu-text"> Dashboard </span>
</a>

<b class="arrow"></b>
</li>
@if(Sentinel::getUser()->roles()->first()->slug =='admin' OR Sentinel::getUser()->roles()->first()->slug =='subadmin')
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
	Button Show/Hide
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		<!--	<li class="">-->
		<!--<a href="/buttons/create">-->
		<!--<i class="menu-icon fa fa-caret-right"></i>-->
		<!--Add -->
		<!--</a>-->

		<!--<b class="arrow"></b>-->
		<!--</li>-->
    
		<li class="">
		<a href="/buttons">
		<i class="menu-icon fa fa-caret-right"></i>
		View 
		</a>

		<b class="arrow"></b>
		</li>
    
    


</ul>
</li>

<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Apply for help
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		<li class="">
		<a href="/helps">
		<i class="menu-icon fa fa-caret-right"></i>
		View 
		</a>

		<b class="arrow"></b>
		</li>
    
    <li class="">
		<a href="/help-report">
		<i class="menu-icon fa fa-caret-right"></i>
		Report  
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

 <li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Project
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class=""> 
		<a href="/projects/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Create Project
		</a>

		<b class="arrow"></b>
		</li>

		<li class="">
		<a href="/projects">
		<i class="menu-icon fa fa-caret-right"></i>
		View Project
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>  


		<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Slider
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class=""> 
		<a href="/sliders/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Slider
		</a>

		<b class="arrow"></b>
		</li>

		<li class="">
		<a href="/sliders">
		<i class="menu-icon fa fa-caret-right"></i>
		View Silder
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
	<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Foundation Overview
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class=""> 
		<a href="/foundationoverviews/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add 
		</a>

		<b class="arrow"></b>
		</li>

		<li class="">
		<a href="/foundationoverviews">
		<i class="menu-icon fa fa-caret-right"></i>
		View 
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Unit Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class=""> 
		<a href="/units/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Create Unit
		</a>

		<b class="arrow"></b>
		</li>

		<li class="">
		<a href="/units">
		<i class="menu-icon fa fa-caret-right"></i>
		View Unit
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Activities Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/activities/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Activities
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/activities">
		<i class="menu-icon fa fa-caret-right"></i>
		View Activities
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

		<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Division Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/divisions/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Division
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/divisions">
		<i class="menu-icon fa fa-caret-right"></i>
		View Division
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

		<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		District Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/districts/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add District
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/districts">
		<i class="menu-icon fa fa-caret-right"></i>
		View Districts
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Upozila Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/locations/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Upozila
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/locations">
		<i class="menu-icon fa fa-caret-right"></i>
		View Upozila
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Union Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/unions/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Union
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/unions">
		<i class="menu-icon fa fa-caret-right"></i>
		View Union
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Members Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/members/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Member
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/members">
		<i class="menu-icon fa fa-caret-right"></i>
		View Members
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Main Program
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/mainprograms/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Main Program
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/mainprograms">
		<i class="menu-icon fa fa-caret-right"></i>
		View Main Program
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Sub Program
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/subprograms/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Sub Program
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/subprograms">
		<i class="menu-icon fa fa-caret-right"></i>
		View Sub Program
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>

<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
	Child Program
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/childprograms/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add Child program
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/childprograms">
		<i class="menu-icon fa fa-caret-right"></i>
		View Child program
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
About Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">
		
		 

		<li class="">
		<a href="/abouts/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add about
		</a>

		<b class="arrow"></b>
		</li>
		
		<li class="">
		<a href="/abouts">
		<i class="menu-icon fa fa-caret-right"></i>
		View about
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		User Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">

		<li class="">
		<a href="/user">
		<i class="menu-icon fa fa-caret-right"></i>
	   User list
		</a>
		<b class="arrow"></b>
		</li>
		<li class="">
		<a href="/password-reset">
		<i class="menu-icon fa fa-caret-right"></i>
	   Password Reset
		</a>
		<b class="arrow"></b>
		</li>
		<li class="">
		<a href="/contacts">
		<i class="menu-icon fa fa-caret-right"></i>
	   Contact list
		</a>
		<b class="arrow"></b>
		</li>
 
</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Contact Address
		</span>
		<b class="arrow fa fa-angle-down"></b>
		</a>
		<ul class="submenu">
		<li class="">
		<a href="/contactaddresses/create">
		<i class="menu-icon fa fa-caret-right"></i>
	   Add 	Contact Address
		</a>
		<b class="arrow"></b>
		</li>
		<li class="">
		<a href="/contactaddresses">
		<i class="menu-icon fa fa-caret-right"></i>
	   View Contact Address
		</a>
		<b class="arrow"></b>
		</li>
 
</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Category 
		</span>
		<b class="arrow fa fa-angle-down"></b>
		</a>
		<ul class="submenu">
		<li class="">
		<a href="/categories/create">
		<i class="menu-icon fa fa-caret-right"></i>
	   Add 	Category
		</a>
		<b class="arrow"></b>
		</li>
		<li class="">
		<a href="/categories">
		<i class="menu-icon fa fa-caret-right"></i>
	   View Category
		</a>
		<b class="arrow"></b>
		</li>
 
</ul>
</li>
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Policy Section
		</span>
		<b class="arrow fa fa-angle-down"></b>
		</a>
		<ul class="submenu">
		<li class="">
		<a href="/polices/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Add new Policy
		</a>
		<b class="arrow"></b>
		</li>
		<li class="">
		<a href="/polices">
		<i class="menu-icon fa fa-caret-right"></i>
		View Policy
		</a>
		<b class="arrow"></b>
		</li>
</ul>
</li>
@endif
@if(Sentinel::getUser()->roles()->first()->slug =='manager')
<li class="">
		<a href="#" class="dropdown-toggle">
		<i class="menu-icon fa fa-desktop"></i>
		<span class="menu-text">
		Unit Section
		</span>

		<b class="arrow fa fa-angle-down"></b>
		</a>

		<ul class="submenu">

		<li class=""> 
		<a href="/units/create">
		<i class="menu-icon fa fa-caret-right"></i>
		Create Unit
		</a>

		<b class="arrow"></b>
		</li>

		<li class="">
		<a href="/units">
		<i class="menu-icon fa fa-caret-right"></i>
		View Unit
		</a>

		<b class="arrow"></b>
		</li>


</ul>
</li>
@endif
</ul><!-- /.nav-list -->

<div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
<i id="sidebar-toggle-icon" class="ace-icon fa fa-angle-double-left ace-save-state" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
</div>
</div>

<div class="main-content">
<div class="main-content-inner">
<div class="breadcrumbs ace-save-state" id="breadcrumbs">
<ul class="breadcrumb">
<li>
<i class="ace-icon fa fa-home home-icon"></i>
<a href="/" target="blank_page">Home</a>
</li>
</ul><!-- /.breadcrumb -->

<div class="nav-search" id="nav-search">
<form class="form-search">
<span class="input-icon">
<input type="text" placeholder="Search ..." class="nav-search-input" id="nav-search-input" autocomplete="off" />
<i class="ace-icon fa fa-search nav-search-icon"></i>
</span>
</form>
</div><!-- /.nav-search -->
</div>
