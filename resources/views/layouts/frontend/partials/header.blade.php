<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if IE 9 ]><html class="ie ie9" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->
<head>

	<!-- Basic Page Needs -->
	<meta charset="utf-8">
	<title>@yield('title')</title>
	<meta name="description" content="Ask me Responsive Questions and Answers Template">
	<meta name="author" content="2code.info">
	
	<!-- Mobile Specific Metas -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
	
	<!-- Main Style -->
	<link rel="stylesheet" href="{{asset('frontend/style.css')}}">
	
	<!-- Skins -->
	<link rel="stylesheet" href="{{asset('frontend/css/skins/skins.css')}}">
	
	<!-- Responsive Style -->
	<link rel="stylesheet" href="{{asset('frontend/css/responsive.css')}}">
	
	<!-- Favicons -->
	<link rel="shortcut icon" href="http://2code.info/demo/html/ask-me/images/favicon.ico">

	  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
   
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  @yield('css')
  
</head>
<body>

<div class="loader"><div class="loader_html"></div></div>

<div id="wrap">
	
	<div class="login-panel">
		<section class="container">
			<div class="row">
				<div class="col-md-6">
					<div class="page-content">
						<h2>Login</h2>
						<div class="form-style form-style-3">
							<form>
								<div class="form-inputs clearfix">
									<p class="login-text">
										<input type="text" value="Username" onfocus="if (this.value == 'Username') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Username';}">
										<i class="icon-user"></i>
									</p>
									<p class="login-password">
										<input type="password" value="Password" onfocus="if (this.value == 'Password') {this.value = '';}" onblur="if (this.value == '') {this.value = 'Password';}">
										<i class="icon-lock"></i>
										<a href="#">Forget</a>
									</p>
								</div>
								<p class="form-submit login-submit">
									<input type="submit" value="Log in" class="button color small login-submit submit">
								</p>
								<div class="rememberme">
									<label><input type="checkbox" checked="checked"> Remember Me</label>
								</div>
							</form>
						</div>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
				<div class="col-md-6">
					<div class="page-content Register">
						<h2>Register Now</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi adipiscing gravdio, sit amet suscipit risus ultrices eu. Fusce viverra neque at purus laoreet consequa. Vivamus vulputate posuere nisl quis consequat.</p>
						<a class="button color small signup">Create an account</a>
					</div><!-- End page-content -->
				</div><!-- End col-md-6 -->
			</div>
		</section>
	</div><!-- End login-panel -->
	
	<div class="panel-pop" id="signup">
		<h2>Register Now<i class="icon-remove"></i></h2>
		<div class="form-style form-style-3">
			<form action="/sign-up" method="POST">
				{{csrf_field()}}
				<div class="form-inputs clearfix">
					<p>
						<label class="required">First name<span>*</span></label>
						<input type="text" name="first_name" placeholder="first name">
					</p>
					<p>
						<label class="required">Last name<span>*</span></label>
						<input type="text" name="last_name" placeholder="last name">
					</p>
					<p>
						<label class="required">E-Mail<span>*</span></label>
						<input type="text" name="email" placeholder="email/mobile number">
					</p>
					<p>
						<label class="required">Password<span>*</span></label>
						<input type="password" name="password" placeholder="password">
					</p>
					<p>
						<label class="required">Confirm Password<span>*</span></label>
						<input type="password" name="confirm_password" placeholder="confirm password">
					</p>
				</div>
				<p class="form-submit">
					<input type="submit" value="Signup" class="button color small submit">
				</p>
			</form>
		</div>
	</div><!-- End signup -->
	
	 
	
	<div id="header-top">
		<section class="container clearfix">
			<nav class="header-top-nav">
				<ul>
					<li><a href="contact_us.html"><i class="icon-envelope"></i>Contact</a></li>
					<li><a href="#"><i class="icon-headphones"></i>Support</a></li>
					<li><a href="login.html" id="login-panel"><i class="icon-user"></i>Login Area</a></li>
				</ul>
			</nav>
			<div class="header-search">
				<form>
				    <input type="text" value="Search here ..." onfocus="if(this.value=='Search here ...')this.value='';" onblur="if(this.value=='')this.value='Search here ...';">
				    <button type="submit" class="search-submit"></button>
				</form>
			</div>
		</section><!-- End container -->
	</div><!-- End header-top -->
	<header id="header">
		<section class="container clearfix">
			<div class="logo"><a href="index.html"><img alt="" src="{{asset('frontend/images/logo.png')}}"></a></div>
			<nav class="navigation">
				<ul>
					<li class="current_page_item"><a href="index.html">Home</a>
						 
					</li>
					<li class="ask_question"><a href="/questions/create">Ask Question</a></li>
					 
					

					@if(Sentinel::check())
					<li><a href="user_profile.html">User</a>
						<ul>
							<li><a href="user_profile.html">My Profile</a></li>
							<li><a href="/questions">My Questions</a></li>
							<li><a href="user_answers.html">My Answers</a></li>
							 
							<li><a href="edit_profile.html">Edit Profile</a></li>
						</ul>
					</li>
                   <li><a href="/logout">Logout</a></li>
					@else

					<li><a href="/login">Login</a></li>

					@endif
					 
					 
					 
				</ul>
			</nav>
		</section><!-- End container -->
	</header><!-- End header -->
    <br>
	@include('frontend.messages.message')