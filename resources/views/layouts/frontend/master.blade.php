@include('layouts.frontend.partials.header')

<div>
@yield('content')
</div>

@include('layouts.frontend.partials.footer')