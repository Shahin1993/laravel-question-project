<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Departments;
class DepartmentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       $dpt=Departments::orderBy('id','desc')->paginate(10);
        return view('department.view',compact('dpt'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $dpt=Departments::all();
        return view('department.create',compact('dpt'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
    'dpt_name' => 'required|unique:departments|min:3|max:255',
    'dpt_code' => 'required',
]);
        $data=$request->all();
        Departments::create($data);
        return back()->with('success','Data insert Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data=Departments::find($id);
        return view('department.edit',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $request->validate([
    'dpt_name' => 'required|min:3|max:255',
    'dpt_code' => 'required',
]);
        $data=Departments::find($id);
        $new_data=$request->all();
        $data->update($new_data);
        return back()->with('success','Data Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data=Departments::find($id);
        $data->delete();
         return back()->with('success','Data Deleted Successfully');
    }
}
