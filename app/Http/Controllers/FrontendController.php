<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Questions;
class FrontendController extends Controller
{
    public function index()
    {
    	$questions=Questions::orderBy('id','desc')->get();
    	return view('frontend.index',compact('questions'));
    }
}
