<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Users;
use Sentinel;
class RegistrationController extends Controller
{
    public function signUp()
    {
    	return view('authentication.sign_up');
    }

    public function signUpPost(Request $request)
    {
    	$request->validate([
    'first_name' => 'required|max:70',
    'last_name' => 'required|max:70',
    'email' =>  'required|unique:users',
    
    'password' => 'required|min:4',
    'confirm_password' => 'required_with:password|same:password|min:4',
]);
      $user=Sentinel::registerAndActivate($request->all());
      $role =Sentinel::findRoleBySlug('member');
      $role->users()->attach($user);
      return redirect()->back()->with('success','Registration successful');
    }
}
