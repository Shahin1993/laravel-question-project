<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Sentinel;
class ChangepasswordController extends Controller
{
    public function create()
    {
    	return view('authentication.change_password');
    }
    public function store(Request $request)
    {
    	 $request->validate([
    	 	'old_password'=>'required',
    'password' => 'required|min:4',
    'confirm_password' => 'required|min:4',
]);
       $hasher=Sentinel::getHasher();
       $old_password=$request->old_password;
       $password=$request->password;
       $confirm_password=$request->confirm_password;
       $user=Sentinel::getUser();
       if (!$hasher->check( $old_password, $user->password)) {
       return redirect()->back()->with('error','old password not match');
       }
       elseif ($password != $confirm_password) {
       	return redirect()->back()->with('error','New password and confirm password not match');
       }
       else{
       	Sentinel::update($user,array('password'=>$password
       	));
       	return redirect()->back()->with('success','Password change Successfully');
       }
    }
}
