<?php


Route::get('/','FrontendController@index');
 


Route::get('/departments/create','DepartmentController@create');
Route::post('/departments','DepartmentController@store');
Route::get('/departments','DepartmentController@index');
Route::get('/departments/{id}/edit','DepartmentController@edit');
Route::post('/departments/{id}','DepartmentController@update');
Route::get('/department-delete/{id}','DepartmentController@destroy');

Route::get('/sign-up','RegistrationController@signUp');
Route::post('/sign-up','RegistrationController@signUpPost');

Route::get('/login','LoginController@login');
Route::post('/login','LoginController@loginPost');
Route::get('/logout','LoginController@logout');

Route::get('/password-change','ChangepasswordController@create');
Route::post('/password-change','ChangepasswordController@store');
//admin dashboard
Route::get('/dashboard','AdminController@dashboard');

//question
Route::get('/questions/create','User\QuestionController@create');
Route::post('/questions','User\QuestionController@store');
Route::get('/questions','User\QuestionController@index');
Route::get('/questions/{id}/edit','User\QuestionController@edit');
Route::post('/questions/{id}','User\QuestionController@update');

Route::get('/questionDetails/{id}','Frontend\QuestionController@show');